#!/bin/sh

DIR_NAME=/usr/share/tuxguitar

JAVA=`which java`

LD_LIBRARY_PATH="$LD_LIBRARY_PATH":/usr/lib
LD_LIBRARY_PATH="$LD_LIBRARY_PATH":/usr/lib/jni

CLASSPATH="$CLASSPATH":/usr/share/java/org.eclipse.swt.jar
CLASSPATH="$CLASSPATH":/usr/share/java/itext5.jar
CLASSPATH="$CLASSPATH":/usr/share/java/itext5-pdfa.jar
CLASSPATH="$CLASSPATH":/usr/share/java/itext5-xmlworker.jar
CLASSPATH="$CLASSPATH":/usr/share/java/commons-compress.jar
CLASSPATH="$CLASSPATH":"$DIR_NAME"/lib/*
CLASSPATH="$CLASSPATH":"$DIR_NAME"/dist/

MAINCLASS=org.herac.tuxguitar.app.TGMainSingleton

"$JAVA" -Xmx512m -cp :"$CLASSPATH" -Dtuxguitar.home.path="$DIR_NAME" -Dtuxguitar.share.path="$DIR_NAME" -Djava.library.path="$LD_LIBRARY_PATH" "$MAINCLASS" "$@"
